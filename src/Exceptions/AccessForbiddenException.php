<?php

namespace Slosh\GoSloshClient\Exceptions;

/**
 * Class NotConfiguredException
 */
class AccessForbiddenException extends \Exception {}