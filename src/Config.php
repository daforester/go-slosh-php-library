<?php

namespace Slosh\GoSloshClient;

class Config
{
    var $Name;
    var $Client;
    var $Secret;
    var $EndPoint;
    var $VerifySSL;
}