<?php

namespace Slosh\GoSloshClientExample\Lib\Commands;

use GetOpt\Command;
use GetOpt\GetOpt;
use GetOpt\Option;
use Slosh\GoSloshClient\GoService;
use Slosh\GoSloshClientExample\Lib\Traits\Configurable;

class GetClientToken extends Command {

    use Configurable;

    public function __construct()
    {
        $options = [
            Option::create("u", "user", Getopt::REQUIRED_ARGUMENT)->setDescription("Specify user to act on behalf of"),
        ];

        parent::__construct('getclienttoken', [$this, 'handle'], $options);
    }

    /**
     * @param GetOpt $opt
     */
    public function handle(GetOpt $opt)
    {
        try {
            $c = $this->getSloshConfig($opt);
        } catch (\Exception $exception) {
            trigger_error("Invalid configuration: " . $exception->getMessage());
            return;
        }

        try {
            $cs = GoService::NewGoService($c, false, $opt->getOption("user"));
            $r = $cs->GetClientToken();

            if ($r) {
                echo "\nToken: ", $r->access, "\nExpires: ", $r->accessExpires, "\n";
                echo "\nRefresh Token: ", $r->refresh, "\nExpires: ", $r->refreshExpires, "\n";
            } else {
                echo "\nNo token retrieved.\n";
            }
        } catch (\Exception $exception) {
            trigger_error("Unexpected error occurred: " . $exception->getMessage());
        }
    }
}