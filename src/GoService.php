<?php

namespace Slosh\GoSloshClient;

use Slosh\GoSloshClient\Exceptions\AccessForbiddenException;
use Slosh\GoSloshClient\Exceptions\APIRequestException;
use Slosh\GoSloshClient\Exceptions\APIUnavailableException;
use Slosh\GoSloshClient\Exceptions\ClientTokenDecodingException;
use Slosh\GoSloshClient\Exceptions\InvalidJSONResponseException;
use Slosh\GoSloshClient\Exceptions\NotConfiguredException;
use Slosh\GoSloshClient\Exceptions\TokenNotFoundException;
use ReflectionException;

/**
 * Class GoService
 * @package DaForester\GoSloshClient
 */
class GoService
{
    /**
     * @var GoService
     */
    protected static $defaultInstance;

    /**
     * @var GoService[]
     */
    protected static $instances = [];

    /** @var Config */
    protected $config;

    /** @var string */
    protected $user = "";

    /**
     * @param Config $config
     * @param bool   $isDefault
     * @param string $user
     *
     * @return GoService
     */
    public static function NewGoService(Config $config, $isDefault = false, $user = "")
    {
        $cs = new GoService();
        $cs->SetConfig($config);

        if (is_null(self::$defaultInstance) || $isDefault) {
            self::$defaultInstance = $cs;
        }

        if (strlen($config->Name) > 0) {
            self::$instances[$config->Name] = $cs;
        }

        if (strlen($user) > 0) {
            $cs->SetUser($user);
        }

        return $cs;
    }

    /**
     * @param string $name
     *
     * @return GoService|null
     * @throws NotConfiguredException
     */
    public static function GetInstance($name = '')
    {
        if (strlen($name) > 0) {
            if (isset(self::$instances[$name])) {
                return self::$instances[$name];
            } else {
                throw new NotConfiguredException("Calendar Service " . $name . " not configured");
            }
        }

        if (isset(self::$defaultInstance)) {
            return self::$defaultInstance;
        }

        throw new NotConfiguredException("Calendar Service not configured");
    }

    /**
     * @param Config $config
     */
    protected function SetConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $user
     */
    public function SetUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return ClientToken|mixed|null
     * @throws APIRequestException
     * @throws TokenNotFoundException
     * @throws ReflectionException
     */
    public function GetClientToken() {
        try {
            if ($this->user) {
                $json = '{"ClientUser": "' . $this->user . '"}';
                $r = $this->curlRequest("/client/token", $json);
            } else {
                $r = $this->curlRequest("/client/token");
            }
        } catch(\Exception $e) {
            throw new APIRequestException("Failed to access API");
        }

        if ($r === null) {
            return null;
        }

        if (!isset($r->access)) {
            throw new TokenNotFoundException("Access Token not found in JSON response");
        }

        if (!isset($r->refresh)) {
            throw new TokenNotFoundException("Refresh Token not found in JSON response");
        }

        return ClientToken::FromStdClass($r);
    }

    /**
     * @param string $r
     *
     * @return object|null
     * @throws APIRequestException
     * @throws ClientTokenDecodingException
     * @throws InvalidJSONResponseException
     */
    protected function decodeResponse($r) {
        if ($r === null) {
            return null;
        }

        $o = json_decode($r);

        if ($o === null) {
            throw new ClientTokenDecodingException("Error decoding JSON response for client token");
        }

        if (!isset($o->ResponseCode) || !isset($o->Response)) {
            throw new InvalidJSONResponseException("Invalid JSON response");
        }

        if ($o->ResponseCode !== 200) {
            throw new APIRequestException("API request failed", $o->ResponseCode);
        }

        return (object) $o->Response;
    }

    /**
     * @param string $url
     * @param string $json
     *
     * @return object|null
     * @throws APIRequestException
     * @throws APIUnavailableException
     * @throws AccessForbiddenException
     * @throws ClientTokenDecodingException
     * @throws InvalidJSONResponseException
     */
    protected function curlRequest($url, $json = "") {
        $ch = curl_init($this->config->EndPoint . $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->config->Client . ":" . $this->config->Secret);

        $headers = [];

        if (strlen($json) > 0) {
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Content-Length: ' . strlen($json);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        } else if (count($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if (!$this->config->VerifySSL) {
            curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
            if (defined('CURLOPT_SSL_VERIFYSTATUS')) {
                curl_setopt($ch,CURLOPT_SSL_VERIFYSTATUS, false);
            }
        }

        //curl_setopt($ch, CURLOPT_VERBOSE, true);

        $r = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if (is_bool($r)) {
            if ($r === false) {
                throw new APIRequestException("Error making API request");
            } else {
                throw new APIRequestException("No response data for API request");
            }
        }

        switch ($httpcode) {
            case 503:
                throw new APIUnavailableException("API Service Unavailable");
            case 403:
                throw new AccessForbiddenException("Access Forbidden");
            case 400:
                throw new APIRequestException("Bad API request");
            case 200:
                return $this->decodeResponse($r);
            default:
                throw new APIRequestException("Unhandled API response", $httpcode);
        }
    }
}