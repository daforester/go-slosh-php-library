<?php

namespace Slosh\GoSloshClientExample\Lib\Traits;

use GetOpt\GetOpt;
use Yosymfony\Toml\Toml;
use Slosh\GoSloshClient\Config;

trait Configurable {

    /**
     * @param GetOpt $opt
     *
     * @return Config
     */
    protected function getSloshConfig(GetOpt $opt) {
        $c = new Config();

        list($client, $secret, $endpoint, $verify_ssl) = $this->getConfigOptions($opt);

        $c->Name = "Example";
        $c->Client = $client;
        $c->Secret = $secret;
        $c->EndPoint = $endpoint;
        $c->VerifySSL = $verify_ssl;

        return $c;
    }

    /**
     * @param GetOpt $opt
     *
     * @return array
     */
    protected function getConfigOptions(GetOpt $opt) {
        $config = $opt->getOption('config');

        $client = "";
        $secret = "";
        $endpoint = "";
        $verify_ssl = false;

        if (is_null($config)) {
            $client = $opt->getOption('client');
            $secret = $opt->getOption('secret');
            $endpoint = $opt->getOption('endpoint');
            $verify_ssl = $opt->getOption('verify_ssl');

            if (!$client || !$secret || !$endpoint) {
                throw new \InvalidArgumentException("client, secret & endpoint must be set if not providing config file");
            }
        } else {
            $c = Toml::parseFile($config);

            if (!$c['CLIENT'] || !$c['SECRET'] || !$c['ENDPOINT']) {
                throw new \InvalidArgumentException("client, secret & endpoint must be set if not providing config file");
            }

            $client = $c['CLIENT'];
            $secret = $c['SECRET'];
            $endpoint = $c['ENDPOINT'];
            $verify_ssl = $c['VERIFY_SSL'];
        }

        return [ $client, $secret, $endpoint, $verify_ssl ];
    }

}