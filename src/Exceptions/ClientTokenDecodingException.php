<?php

namespace Slosh\GoSloshClient\Exceptions;

/**
 * Class NotConfiguredException
 */
class ClientTokenDecodingException extends CalendarException {}