<?php

namespace Slosh\GoSloshClient\Exceptions;

/**
 * Class NotConfiguredException
 */
class NotConfiguredException extends CalendarException {}