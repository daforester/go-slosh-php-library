<?php

namespace Slosh\GoSloshClient\Exceptions;

/**
 * Class NotConfiguredException
 */
class APIUnavailableException extends \Exception {}