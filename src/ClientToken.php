<?php

namespace Slosh\GoSloshClient;

use Slosh\GoSloshClient\Traits\Castable;

use \stdClass;
use \ReflectionException;

class ClientToken
{
    use Castable;

    var $access;
    var $accessExpires;
    var $refresh;
    var $refreshExpires;

    /**
     * @param stdClass $s
     * @return ClientToken|mixed
     * @throws ReflectionException
     */
    public static function FromStdClass(\stdClass $s)
    {
        $d = new ClientToken();

        $d = self::Cast($s, $d);

        return $d;
    }
}