<?php

namespace Slosh\GoSloshClient\Exceptions;

/**
 * Class NotConfiguredException
 */
class InvalidJSONResponseException extends CalendarException {}